package com.dtb.angelplayer.MongoDBMusicClasses;

import org.mongodb.morphia.converters.TypeConverter;
import org.mongodb.morphia.mapping.MappedField;

import java.time.Duration;

/**
 * Created by beij on 24.07.16.
 */
public class DurationConverter extends TypeConverter{
    @Override
    public Object decode(Class<?> aClass, Object o, MappedField mappedField) {
        return null;
    }

    public DurationConverter() {
        super(Duration.class);
    }
}
