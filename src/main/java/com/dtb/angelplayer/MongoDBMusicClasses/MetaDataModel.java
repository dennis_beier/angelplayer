package com.dtb.angelplayer.MongoDBMusicClasses;

import com.dtb.angelplayer.EngineNg.Core.EngineMetadataSubject;
import com.dtb.angelplayer.Gui.MusicMetadata;
import com.mongodb.MongoClient;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by beij on 24.07.16.
 */
public class MetaDataModel implements EngineMetadataSubject {
    protected static MetaDataModel instance;
    private MongoClient client  = new MongoClient();
    private Morphia morphia = new Morphia();
    protected Datastore datastore = morphia.createDatastore(client, "musicdata");
    public static MetaDataModel getInstance(){
        if (instance == null) {
            instance = new MetaDataModel();
        }
        return instance;
    }
    public void newEntry(MusicMetadata metadata){
        try {
            datastore.save(metadata);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void loadMusic() throws ExecutionException, InterruptedException {
        List<MusicMetadata> list = datastore.find(MusicMetadata.class).asList();
        this.notify(list);
    }
}
