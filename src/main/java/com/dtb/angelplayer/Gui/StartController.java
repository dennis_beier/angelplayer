package com.dtb.angelplayer.Gui;

import java.io.IOException;
import java.net.URL;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import com.dtb.angelplayer.Gui.model.MusicMetadataModel;

public class StartController {

	@FXML
	public void handleMusicClickEvent(ActionEvent event) throws IOException {
		MusicMetadataModel.getInstance();
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		URL ressource = getClass().getClassLoader().getResource("javafx-views/music.fxml");
		Parent root = FXMLLoader.load(ressource);
		Scene scene2 = new Scene(root);
		stage.setScene(scene2);
		stage.show();
	}
}
