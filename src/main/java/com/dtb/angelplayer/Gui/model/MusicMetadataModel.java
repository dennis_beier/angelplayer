package com.dtb.angelplayer.Gui.model;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.stream.Stream;

import com.dtb.angelplayer.EngineNg.Engine;
import com.dtb.angelplayer.EngineNg.Core.EngineListener;
import com.dtb.angelplayer.MongoDBMusicClasses.MetaDataModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import com.dtb.angelplayer.Gui.MusicMetadata;

public class MusicMetadataModel {

	ArrayList<MusicMetadata> list = new ArrayList<>();
	ObservableList<MusicMetadata> observableList = FXCollections.observableArrayList(this.list);
	
	static MusicMetadataModel instance;

	private static MetaDataModel model = MetaDataModel.getInstance();
	public ObservableList<MusicMetadata> getObservableList() {
		return this.observableList;
	}

	protected MusicMetadataModel() {
		Engine.getInstance().register(new ListenerForNewMusicData());
	}

	protected class ListenerForNewMusicData implements EngineListener {
		@Override
		public void onNewMusicFile(MusicMetadata metadata) throws ExecutionException, InterruptedException {
			MusicMetadataModel.this.observableList.add(metadata);
			model.newEntry(metadata);
		}
	}

	public static MusicMetadataModel getInstance() {
		if (instance == null) {
			instance = new MusicMetadataModel();
		}
		return instance;
	}

	/**
	 * Dude Holy shit dude, motherfucker dude
	 * @param strings strings
	 * @return void
	 */
	public Stream<MusicMetadata> fullTextSearch( String... strings  ) {
		return this.getObservableList().stream().filter( ( metadata ) -> {
			return Stream.of( strings ).anyMatch( (searchWord) -> {
				return metadata.getAlbum().contains(searchWord)
					|| metadata.getArtist().contains(searchWord)
					|| metadata.getTitle().contains(searchWord);
			});
		});
	}

	/**
	 * Warm up the model
 	 */
	public void loadAllMusicfilesFromDB() throws ExecutionException, InterruptedException {
		model.register(new ListenerForNewMusicData());
		model.loadMusic();
	}
}
