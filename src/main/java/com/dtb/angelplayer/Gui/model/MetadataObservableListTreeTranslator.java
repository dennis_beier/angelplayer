package com.dtb.angelplayer.Gui.model;

import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;
import com.dtb.angelplayer.Gui.MusicMetadata;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

public class MetadataObservableListTreeTranslator implements ListChangeListener<MusicMetadata> {
    // TODO: Etwas besseres ausdenken, da der Cache zwar sein werk tut, aber nicht wiklich guit design ist
    // TODO: Mir wird gerade bewusst, das ein überstzer von Abservable-list zu TreeItem besser ist!

    TreeItem<Map.Entry<String,MusicMetadata>> rootItem = new TreeItem<>(new AbstractMap.SimpleEntry<String, MusicMetadata>("Music", null));
    HashMap<String, TreeItem<String>> artistMap = new HashMap<>();
    HashMap<String, TreeItem<String>> albumMap= new HashMap<>();
    static TreeItem<Map.Entry<String,MusicMetadata>> getOrCreateNode(TreeItem<Map.Entry<String,MusicMetadata>>  treeItem, String nodeName,MusicMetadata data) {
        TreeItem<Map.Entry<String,MusicMetadata>>  nodeTreeItem = null;
        for(TreeItem<Map.Entry<String,MusicMetadata>> current :treeItem.getChildren()){
            if(current.getValue().getKey().equalsIgnoreCase(nodeName)){
                nodeTreeItem = current;
                break;
            }
        }
        if (nodeTreeItem == null) {
            nodeTreeItem = new TreeItem<>(new AbstractMap.SimpleEntry<>(nodeName,data));
            treeItem.getChildren().add(nodeTreeItem);
        }
        return nodeTreeItem;
    }

    @Override
    public void onChanged(javafx.collections.ListChangeListener.Change<? extends MusicMetadata> change) {
        while (change.next()) {
            for (MusicMetadata metadata : change.getAddedSubList()) {
                addToNodeTree(metadata);
            }
            for (MusicMetadata metadata : change.getRemoved()) {
                removeFromNodeTree(metadata);
            }
        }
    }

    private void addToNodeTree(MusicMetadata metadata) {
        TreeItem<Map.Entry<String,MusicMetadata>>  artistNode = getOrCreateNode(rootItem, metadata.getArtist(),null);
        TreeItem<Map.Entry<String,MusicMetadata>>  albumNode = getOrCreateNode(artistNode, metadata.getAlbum(),null);
        TreeItem<Map.Entry<String,MusicMetadata>>  titelNode = getOrCreateNode(albumNode, metadata.getTitle(),metadata);
    }

    private void removeFromNodeTree(MusicMetadata metadata) {
        // TODO: AUF JEDENFALL MAL BESSER MACHEN!
        // HOLY SHIT SGH
        int artistIndex = this.rootItem.getChildren().indexOf(metadata.getArtist());
        if (artistIndex >= 0) {
            TreeItem<Map.Entry<String,MusicMetadata>>  artistNode = rootItem.getChildren().get(artistIndex);
            int albumIndex = artistNode.getChildren().indexOf(metadata.getAlbum());
            if (albumIndex >= 0) {
                TreeItem<Map.Entry<String,MusicMetadata>> albumNode = rootItem.getChildren().get(albumIndex);
                int titelIndex = albumNode.getChildren().indexOf(metadata.getTitle());
                if (titelIndex >= 0) {
                    albumNode.getChildren().remove(titelIndex);
                    if (albumNode.getChildren().isEmpty()) {
                        artistNode.getChildren().remove(albumIndex);
                        if (artistNode.getChildren().isEmpty()) {
                            this.rootItem.getChildren().remove(artistIndex);
                        }
                    }
                }
            }
        }
    }

    public TreeItem<Map.Entry<String,MusicMetadata>> getRootItem() {
        return this.rootItem;
    }

    public void initializeData(ObservableList<MusicMetadata> list) {
        for (MusicMetadata metadata : list) {
            this.addToNodeTree(metadata);
        }
    }
}
