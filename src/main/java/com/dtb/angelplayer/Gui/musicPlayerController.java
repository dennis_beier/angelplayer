package com.dtb.angelplayer.Gui;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.Future;
import java.util.stream.Stream;

import com.dtb.angelplayer.EngineNg.Engine;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import com.dtb.angelplayer.Gui.model.MetadataObservableListTreeTranslator;
import com.dtb.angelplayer.Gui.model.MusicMetadataModel;

public class musicPlayerController implements Initializable {

    public TextField lengthField;
    FilteredList<MusicMetadata> filteredList;
	SortedList<MusicMetadata> sortedList;
	
    @FXML
    public Slider positionSlider;
    MusicMetadata currentSelection = null;
    public GridPane gridPane;
    protected ObservableList<MusicMetadata> observableList = MusicMetadataModel.getInstance().getObservableList();
    protected Vector<Future<MusicMetadata>> futureVector = new Vector<>();
    @FXML
    public TableView<MusicMetadata> tableView;
    @FXML
    private TableColumn<MusicMetadata, String> musicColumn = new TableColumn<>("hallo");
    @FXML
    private HBox hBox;
    
    @FXML
    private TreeView<Map.Entry<String,MusicMetadata>> treeView;
    @FXML
    private TextField spotlightSearchTextfield;
    private DateTimeFormatter endtime = DateTimeFormatter.ofPattern("HH:mm:ss");
    private Timer songTimer = new Timer();
    private Duration currentTimeCode = Duration.ZERO;
    @FXML
    private void initialize() {
    	// INITILISE THE OBSERVABLE_FILDERED LIST.
    	filteredList = new FilteredList<MusicMetadata>( observableList );
    	sortedList = new SortedList<MusicMetadata>( filteredList );
    	
    	// Inizialsierit die TreeView instanz und verbinden den TreeView mit der Observable list;
    	MetadataObservableListTreeTranslator translator = new MetadataObservableListTreeTranslator();
      	treeView.setRoot( translator.getRootItem() );
        treeView.setCellFactory(new Callback<TreeView<Map.Entry<String, MusicMetadata>>, TreeCell<Map.Entry<String, MusicMetadata>>>() {
            @Override
            public TreeCell<Map.Entry<String, MusicMetadata>> call(TreeView<Map.Entry<String, MusicMetadata>> param) {
                return new TreeCell<Map.Entry<String, MusicMetadata>>(){
                    @Override
                    protected void updateItem(Map.Entry<String, MusicMetadata> item, boolean empty) {
                        super.updateItem(item,empty);
                        if(!empty)
                           setText(item.getKey());
                        else
                            setText(null);
                    }
                };
            }
        });
    	treeView.setShowRoot(false);
    	observableList.addListener(translator);
    	translator.initializeData(observableList);

    	
    	tableView.setItems(sortedList);
        observableList.add(new MusicMetadata("hallo", "welt", "bla", "first_run_jingle.ogg", Duration.ZERO,"xxx"));
        TableColumn<MusicMetadata, String> titleColumn = new TableColumn<>("Title");
        titleColumn.setCellValueFactory(new PropertyValueFactory<MusicMetadata, String>("Title"));
        TableColumn<MusicMetadata, String> artistColumn = new TableColumn<>("Artist");
        artistColumn.setCellValueFactory(new PropertyValueFactory<MusicMetadata, String>("Artist"));
        TableColumn<MusicMetadata, String> albumColumn = new TableColumn<>("Album");
        albumColumn.setCellValueFactory(new PropertyValueFactory<MusicMetadata, String>("Album"));
        TableColumn<MusicMetadata, String> pathColumn = new TableColumn<>("Path");
        pathColumn.setCellValueFactory(new PropertyValueFactory<MusicMetadata, String>("Path"));
        titleColumn.prefWidthProperty().bind(tableView.widthProperty().divide(4)); // w * 1/4
        artistColumn.prefWidthProperty().bind(tableView.widthProperty().divide(4));
        pathColumn.prefWidthProperty().bind(tableView.widthProperty().divide(4));
        albumColumn.prefWidthProperty().bind(tableView.widthProperty().divide(4));
        tableView.getColumns().setAll(titleColumn, artistColumn, albumColumn, pathColumn);
        
        // SET SOME EVENT LISTENER
        // Set listener for spottlicght input
        this.spotlightSearchTextfield.textProperty().addListener((observable, oldValue, newValue) -> {
            newValue = newValue.trim();
            if( !newValue.isEmpty() ) {
                String[] searchWords = newValue.split(" ");
                filteredList.setPredicate( (metadata) -> {
                    return Stream.of( searchWords ).parallel().anyMatch( (searchWord) -> {
                        return metadata.getAlbum().toLowerCase().contains(searchWord.toLowerCase())
                                || metadata.getArtist().toLowerCase().contains(searchWord.toLowerCase())
                                || metadata.getTitle().toLowerCase().contains(searchWord.toLowerCase());
                    });
                });
                sortedList.setComparator(Comparator.comparingLong( (metadata) -> {
                    long rate = Long.MIN_VALUE;

                    for( String keyword : searchWords) {
                        if( metadata.getTitle().contains(keyword) ) {
                            rate += 30;
                        } else if( metadata.getTitle().toLowerCase().contains(keyword.toLowerCase()) ) {
                            rate += 25;
                        }
                        if( metadata.getAlbum().contains(keyword) ) {
                            rate += 20;
                        } else if( metadata.getAlbum().toLowerCase().contains(keyword.toLowerCase()) ) {
                            rate += 15;
                        }
                        if( metadata.getArtist().contains(keyword) ) {
                            rate += 10;
                        } else if( metadata.getArtist().toLowerCase().contains(keyword.toLowerCase()) ) {
                            rate += 5;
                        }
                    }

                    return rate;
                }));
            } else {
                filteredList.setPredicate(null);
                sortedList.setComparator(null);
            }
        });
        // set event listerner for new slider position
        positionSlider.valueProperty().addListener((ObservableValue<? extends Number> ov, Number oldValue, Number newValue) -> {
            double position = newValue.doubleValue();
            JMplayer.getInstance().searchPosition((int) position);
        });
    }

    public void toggleTrack(ActionEvent actionEvent) throws IOException {
        System.out.println("toggle!");
        JMplayer.getInstance().tooglePause();
    }

    public void tableCellClicked(Event event) throws IOException {
        MusicMetadata currentSelection = tableView.getSelectionModel().getSelectedItem();
        playMusicFile(currentSelection);
    }

    protected void playMusicFile(MusicMetadata currentSelection) throws IOException {
        if (currentSelection != null && currentSelection != this.currentSelection) {
            this.currentSelection = currentSelection;
            JMplayer jmplayer = JMplayer.getInstance();
            jmplayer.loadNewFile(currentSelection.getPath());
            Duration duration = currentSelection.getDuration();
            LocalTime t = LocalTime.MIDNIGHT.plus(duration);
            String s = endtime.format(t);
            startCountDown();
            lengthField.setText(s);
        }

    }

    public void searchFile(ActionEvent actionEvent) throws IOException {
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Choose music directory");
        File defaultDirectory = new File(System.getProperty("user.home"));
        chooser.setInitialDirectory(defaultDirectory);
        File selectedDirectory = chooser.showDialog(stage);
        if (selectedDirectory != null) {
            Engine engine = Engine.getInstance();
            engine.searchDirectory(selectedDirectory.toPath());
        }
    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		this.initialize();
	}

    public void treeCellClicked(Event event) throws IOException {
      TreeItem<Map.Entry<String, MusicMetadata>> treeItem= treeView.getSelectionModel().getSelectedItem();
        if(treeItem != null && treeItem.getValue() != null) {
            MusicMetadata selectedFile = treeItem.getValue().getValue();
            playMusicFile(selectedFile);
        }

    }
    public void startCountDown() {
        currentTimeCode = currentSelection.getDuration();
        songTimer.cancel();
        songTimer.purge();
        songTimer = new Timer();
        songTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> {
                    currentTimeCode = currentTimeCode.minusSeconds(1);
                    LocalTime t = LocalTime.MIDNIGHT.plus(currentTimeCode);
                    String s = endtime.format(t);
                    lengthField.setText(s);

                    if (currentTimeCode.getSeconds() < 0)
                        songTimer.cancel();
                });
            }
        }, 1000, 1000); //Every 1 second
    }
}
