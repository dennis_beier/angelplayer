package com.dtb.angelplayer.Gui;

import java.io.*;
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import java.util.logging.Logger;



public class MplayerWrapper {
	private static final Logger Log = Logger.getLogger("angellogger");

	public final static String MplayerCommand = "mplayer";
	
	private Process mplayerProcess = null;
	private String[] comanndArguments;
	
	private AbstractMap.SimpleEntry<Process, BufferedReader> readStreamCache = new SimpleEntry<Process, BufferedReader>(null, null);
	private AbstractMap.SimpleEntry<Process, OutputStream> writeStreamCache = new SimpleEntry<Process, OutputStream>(null, null);

	
	public MplayerWrapper( String[] commandArguments ) {
		if ( commandArguments == null ) {
			throw new IllegalArgumentException( "commandArguments schould not be null" );
		} else {
			comanndArguments = commandArguments;
		}
	}
	
	public BufferedReader  getMplayerOut() {
		Process mplayerProcess = this.getMplayerProcess();
		if( this.readStreamCache.getKey() == mplayerProcess ) {
			if( this.readStreamCache.getValue() == null ) {
				this.readStreamCache.setValue( getBufferdReader(mplayerProcess) );
			}
		} else {
			if( this.readStreamCache.getKey() != null ) {
				if( this.readStreamCache.getKey().isAlive()) {
					this.readStreamCache.getKey().destroy();
				}
			}
			this.readStreamCache = new SimpleEntry<Process, BufferedReader>(mplayerProcess, getBufferdReader(mplayerProcess));
		}
		return this.readStreamCache.getValue();
	}
	
	public OutputStream getMplayerPipeIn() {
		Process mplayerProcess = this.getMplayerProcess();
		if( this.writeStreamCache.getKey() == mplayerProcess ) {
			if( this.writeStreamCache.getValue() == null ) {
				this.writeStreamCache.setValue( mplayerProcess.getOutputStream() );
			}
		} else {
			if( this.writeStreamCache.getKey() != null ) {
				if( this.writeStreamCache.getKey().isAlive()) {
					this.writeStreamCache.getKey().destroy();
				}
			}
			this.writeStreamCache = new SimpleEntry<Process, OutputStream>(mplayerProcess, mplayerProcess.getOutputStream());
		}
		return this.writeStreamCache.getValue();
	}
	
	private Process getMplayerProcess() {
		try {
			if (this.mplayerProcess == null) {
				this.mplayerProcess = this.createMplayerProcess();
			} else {
				if (!this.mplayerProcess.isAlive()) {
					this.mplayerProcess = this.createMplayerProcess();
				}
			}
		} catch (Exception exception) {
			if (this.mplayerProcess != null) {
				if (!this.mplayerProcess.isAlive()) {
					this.mplayerProcess = null;
				}
			}
		}
		return this.mplayerProcess;
	}
	
	private String[] getExecutingCommand () {
		return this.getExecutingCommand(this.MplayerCommand, this.comanndArguments);
	}
	
	private Process createMplayerProcess() throws IOException {
		return this.mplayerProcess = Runtime.getRuntime().exec(this.getExecutingCommand());
	}
	
	private static String[] getExecutingCommand ( String mplayerCommand, String[] commandArguments ) {
		String[] commandStringArray = new String[ commandArguments.length + 1 ];
		commandArguments[0] = mplayerCommand;
		for(  int index = 0 ; index < commandArguments.length ; ++index ) {
			commandStringArray[index+1] = commandArguments[index]; 
		}
		return commandStringArray;
	}

	private static BufferedReader getBufferdReader(Process process) {
		InputStreamReader tmpStreamReader = new InputStreamReader(process.getInputStream());
		try {
			while (tmpStreamReader.ready()) {
				tmpStreamReader.skip(1000);
			}
		} catch (IOException exception) {
			Log.warning("Error while skip the iniziliesung content in mplayer");
		}
		return new BufferedReader(new InputStreamReader(process.getInputStream()));
	}
}
