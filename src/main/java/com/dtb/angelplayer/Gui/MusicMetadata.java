package com.dtb.angelplayer.Gui;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.concurrent.*;

import javax.xml.bind.DatatypeConverter;

import com.dtb.angelplayer.MongoDBMusicClasses.DurationConverter;
import javafx.beans.property.*;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.NotSaved;
import org.mongodb.morphia.annotations.Converters;

@Entity("Musicfiles")
@Converters(DurationConverter.class)
public class MusicMetadata {
    @Id
    protected ObjectId id;
    private  StringProperty title ;
    private  StringProperty artist;
    private  StringProperty album  ;
    private  StringProperty path;
    @NotSaved
    private  ObjectProperty<Duration> duration;
    private  String durationString;
    @NotSaved
    private Future<byte[]> hashFuture;
    private byte[] hash = null;

    public MusicMetadata(String title, String artist, String album, String path, Duration duration, Future<byte[]> hashFuture) {
        this.title = new SimpleStringProperty(title);
        this.artist = new SimpleStringProperty(artist);
        this.album  = new SimpleStringProperty(album);
        this.path = new SimpleStringProperty(path);
        this.duration = new SimpleObjectProperty<Duration>(duration);
        this.hash = null;
        this.hashFuture = hashFuture;
    }

    public Duration getDuration() {
		return duration.get();
	}

	public String getTitle() {
        return title.get();
    }

    public StringProperty titleProperty() {
        return title;
    }

    public String getArtist() {
        return artist.get();
    }

    public StringProperty artistProperty() {
        return artist;
    }

    public String getAlbum() {
        return album.get();
    }

    public StringProperty albumProperty() {
        return album;
    }

    public MusicMetadata(String title, String artist, String album, String path, Duration duration,String hash ) {
        this.title = new SimpleStringProperty(title);
        this.artist = new SimpleStringProperty(artist);
        this.album  = new SimpleStringProperty(album);
        this.path = new SimpleStringProperty(path);
        this.duration = new SimpleObjectProperty<Duration>(duration);
        this.hash = hash == null ? null:hash.getBytes(StandardCharsets.UTF_8);
    }

    public String getPath() {
        return path.get();
    }

    public StringProperty pathProperty() {
        return path;
    }
    
    public String getHexHash() {
    	return DatatypeConverter.printHexBinary(hash);
    }
    
    public byte[] getHash() {
    	if( this.hash == null) {
    		try {
				this.hash = this.hashFuture.get();
				this.hashFuture = null;
			} catch (InterruptedException | ExecutionException exception) {
				// TODO Auto-generated catch block
				exception.printStackTrace();
			}
    	}
    	return this.hash;
    }

    public MusicMetadata() {
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title.set(title);
    }

    public void setArtist(String artist) {
        this.artist.set(artist);
    }

    public void setAlbum(String album) {
        this.album.set(album);
    }

    public void setPath(String path) {
        this.path.set(path);
    }

    public ObjectProperty<Duration> durationProperty() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration.set(duration);
    }

    public void setHash(byte[] hash) {
        this.hash = hash;
    }
}
