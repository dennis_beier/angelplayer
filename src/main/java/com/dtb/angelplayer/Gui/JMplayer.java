package com.dtb.angelplayer.Gui;

import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.Scanner;

public class JMplayer implements Runnable{
    static JMplayer instance = null;
    protected String filename;
    protected Process mplayer;
    protected PrintStream mplayerPipe;
    protected InputStream mplayerOutput;
    protected BufferedReader mplayerReader;
    protected Scanner mplayerScanner;
    public static JMplayer getInstance(){
        if(instance == null){
            instance = new JMplayer();
            instance.run();
        }
        return instance;
    }
    @Override
    public void run() {
        String[] command = {"mplayer","-slave", "-idle","-quiet"};
        try {
            this.mplayer = Runtime.getRuntime().exec(command);
            mplayerPipe = new PrintStream(mplayer.getOutputStream());
            mplayerReader = new BufferedReader(new InputStreamReader(mplayer.getInputStream()));
            mplayerScanner = new Scanner(mplayerReader);
            mplayerOutput = mplayer.getInputStream();
        } catch (IOException e) {
            System.out.println("Cannot initialize mplayer");
        }
    }
    public void tooglePause(){
        System.out.println("mplayer pauses");
        mplayerPipe.println("pause");
        mplayerPipe.flush();
        System.out.println("paused");
    }
   public void loadNewFile(String file) throws IOException {
       mplayerPipe.print("loadfile \""+file+"\"\n");
       mplayerPipe.flush();
   }
    public Double getLengthOfCurrent() throws IOException {
        mplayerPipe.print("get_property length\n");
        Double time = searchValueInPipe("ANS_length=");
        return time;
    }
    public Double getCurrentTime() throws IOException {
        mplayerPipe.println("get_property length\n");
        Double time = searchValueInPipe("ANS_time_pos=");
        return time;
    }
    public Double getRemainingTime() throws IOException {
        return this.getLengthOfCurrent() -getCurrentTime();
    }
    public Double searchValueInPipe(String match) throws IOException {
        Double totalTime = 0.0;
        String answer;
        while (mplayerOutput.available() != 0 && (answer = mplayerScanner.nextLine()) != null) {
            if (answer.startsWith(match)) {
                totalTime = Double.parseDouble(answer.substring(match.length()));
                break;
            }
        }
        return totalTime;
    }
    public static void printStream(InputStream inputStream) throws IOException {
        String theString = IOUtils.toString(inputStream, "UTF8");
        System.out.println(theString);
    }
    public void searchPosition(int percent){
        mplayerPipe.println("seek "+percent+" 1");
        mplayerPipe.flush();
    }
}

