package com.dtb.angelplayer.EngineNg;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;

import com.dtb.angelplayer.EngineNg.Core.*;
import com.dtb.angelplayer.Gui.MusicMetadata;

public class Engine {
	static private Engine Instance;

	private Set<EngineListener> engineLiseners = new HashSet<EngineListener>();
	private ExecutorService executor = MainThreadPool.getThreadPool();

	public void searchDirectory(Path path) {
		this.searchDirectory(path.toFile());
	}

	public synchronized void register(EngineListener listener) {
		this.engineLiseners.add(listener);
	}

	public void searchDirectory(File file) {
		if (file.isDirectory()) {
			this.DoSearchDirectory(file);
		} else if (file.isFile()) {
			this.DoAnalyseFile(file);
		}
	}

	private void DoAnalyseFile(File file) {
		Runnable runnable = new MusicFileAnalyser(new EngineThrower(), file);
		this.executor.submit(runnable);
	}

	private void DoSearchDirectory(File file) {
		try {
			Files.walkFileTree(file.toPath(), new EngineFileVisitor(new EngineThrower(), this.executor));
		} catch (IOException exception) {
			// TODO Auto-generated catch block
			exception.printStackTrace();
		}
	}

	static public Engine getInstance() {
		if (Instance == null) {
			Instance = new Engine();
		}
		return Instance;
	}

	private synchronized void onFindMediaFile(MusicMetadata metadata) throws ExecutionException, InterruptedException {
		for (EngineListener listener : this.engineLiseners) {
			listener.onNewMusicFile(metadata);
		}
	}

	public class EngineThrower implements EngineEventThrower {
		@Override
		public void newMusicFile(MusicMetadata metadata) throws ExecutionException, InterruptedException {
			Engine.this.onFindMediaFile(metadata);
		}
	}

	private class EngineFileVisitor extends SimpleFileVisitor<Path> {
		private final EngineEventThrower thrower;
		private final ExecutorService executorService;

		public EngineFileVisitor(EngineEventThrower thrower, ExecutorService executorService) {
			this.thrower = thrower;
			this.executorService = executorService;
		}

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attr) throws IOException {
			if (attr.isRegularFile()) {
				this.executorService.submit(new MusicFileAnalyser(this.thrower, file.toFile()));
			}
			return FileVisitResult.CONTINUE;
		}
	}
}
