package com.dtb.angelplayer.EngineNg.Core;

import com.dtb.angelplayer.Gui.MusicMetadata;

import java.util.concurrent.ExecutionException;

public interface EngineEventThrower {
	public void newMusicFile( MusicMetadata metadata ) throws ExecutionException, InterruptedException;
}
