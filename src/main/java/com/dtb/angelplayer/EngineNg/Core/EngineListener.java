package com.dtb.angelplayer.EngineNg.Core;

import com.dtb.angelplayer.Gui.MusicMetadata;

import java.util.concurrent.ExecutionException;

public interface EngineListener {
	void onNewMusicFile( MusicMetadata metadata ) throws ExecutionException, InterruptedException;
}
