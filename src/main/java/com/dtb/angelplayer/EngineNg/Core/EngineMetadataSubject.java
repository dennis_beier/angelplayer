package com.dtb.angelplayer.EngineNg.Core;

import com.dtb.angelplayer.Gui.MusicMetadata;

import java.util.List;
import java.util.Vector;
import java.util.concurrent.ExecutionException;

public interface EngineMetadataSubject {
    Vector<EngineListener> listeners = new Vector<EngineListener>();
    default void register(EngineListener listener) {
            this.listeners.add(listener);
    }
    default void notify(MusicMetadata musicMetadata) throws ExecutionException, InterruptedException {
        for(EngineListener listener :listeners) {
            listener.onNewMusicFile(musicMetadata);
        }
    }
    default void notify(List<MusicMetadata> musicMetadata) throws ExecutionException, InterruptedException {
        for(MusicMetadata metadata: musicMetadata)
            this.notify(metadata);
    }
}
