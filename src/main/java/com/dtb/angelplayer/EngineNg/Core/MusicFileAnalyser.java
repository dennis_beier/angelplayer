package com.dtb.angelplayer.EngineNg.Core;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.time.Duration;
import java.util.Map.Entry;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;
import com.dtb.angelplayer.Gui.JMplayer;
import com.dtb.angelplayer.Gui.MusicMetadata;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class MusicFileAnalyser implements Runnable {
	private static final Logger log = Logger.getLogger(MusicFileAnalyser.class.getName());
	static final String DurationJsonField = "duration";
	static final String StreamsJsonField = "streams";
	static final String TagJsonField = "tags";
	static final String TitleJsonField = "title";
	static final String ArtistJsonField = "artist";
	static final String AlbumJsonField = "album";

	static final ExecutorService hashThreadPool =Executors.newFixedThreadPool(2);

	private final File analysingFile;
	private final EngineEventThrower eventThrower;

	public MusicFileAnalyser(EngineEventThrower thrower, File file) {
		this.eventThrower = thrower;
		this.analysingFile = file;
	}

	private void printStream(InputStream inputStream) throws IOException {
		String theString = IOUtils.toString(inputStream, "UTF8");
		System.out.println(theString);
	}

	private String streamToString(InputStream inputStream) throws IOException {
		String theString = IOUtils.toString(inputStream, "UTF8");
		return theString;
	}

	MusicMetadata toMusicMetadata(JsonObject json, String path, Future<byte[]> hashFuture) {
		if (json.isJsonObject() && json.has("format")) { // TODO: make
															// better
															// if's
			json = json.get("format").getAsJsonObject();
			String album = "";
			String artist = "";
			String title = "";
			Duration duration = Duration.ZERO;
			if (json.has(TagJsonField)) {
				JsonElement tagjson = json.get(TagJsonField);
				if (tagjson.isJsonObject()) {
					JsonObject tags = tagjson.getAsJsonObject();
					for (Entry<String, JsonElement> element : tags.entrySet()) {
						if (element.getKey().equalsIgnoreCase(TitleJsonField)) {
							title= element.getValue().getAsString();
						} else if (element.getKey().equalsIgnoreCase(AlbumJsonField)) {
							album = element.getValue().getAsString();
						} else if (element.getKey().equalsIgnoreCase(ArtistJsonField)) {
							artist = element.getValue().getAsString();
						}
					}
				}
			}
			if (json.has(DurationJsonField)) {
				BigDecimal durationSeconds = (new BigDecimal(json.get(DurationJsonField).getAsString())).stripTrailingZeros();
				BigInteger milisecans = calculateMiliseconds(durationSeconds);
				BigInteger seconds = calculateSeconds(durationSeconds);

				duration = Duration.ofSeconds(seconds.longValueExact());
				duration.plusMillis(milisecans.longValueExact());

			}
			return new MusicMetadata(title, artist, album, path, duration,hashFuture);
		} else {
			throw new IllegalArgumentException("Json is not a Object.");
		}

	}

	BigInteger calculateMiliseconds(BigDecimal durationInSeconds) {
		BigDecimal miliseconds = durationInSeconds.remainder(BigDecimal.ONE);
		miliseconds = miliseconds.scaleByPowerOfTen(miliseconds.scale());
		return miliseconds.toBigIntegerExact();
	}

	BigInteger calculateSeconds(BigDecimal durationInSeconds) {
		BigInteger seconds = durationInSeconds.unscaledValue();
		seconds = seconds.divide(BigInteger.TEN.pow(durationInSeconds.scale()));
		return seconds;
	}

	@Override
	public void run() {
		System.out.println("run");
		if (this.analysingFile.exists()) {
			// String[] command = { "ffprobe", "-print_format",
			// "json","-show_entries", "stream=codec_type",
			// "-of","default=nw=1", "-show_format",
			// this.analysingFile.getAbsolutePath() };
			String[] command = { "ffprobe", "-print_format", "json", "-show_entries", "stream=codec_name:format", "-select_streams", "a:0", "-v", "quiet",analysingFile.getAbsolutePath() };
			try {
				Process process = Runtime.getRuntime().exec(command);
				process.waitFor();
				int errorCode = process.exitValue();
				if (errorCode != 0) {
					System.out.println("Error while executing script");
					JMplayer.printStream(process.getErrorStream());
				} else {
					printStream(process.getErrorStream());
					String json = streamToString(process.getInputStream());
					JsonParser parser = new JsonParser();
					JsonObject jsonObject = parser.parse(json).getAsJsonObject();
					if ( jsonObject.get(StreamsJsonField).getAsJsonArray().size() > 0 ) {
						Future<byte[]> hashFuture = hashThreadPool.submit(new BufferedHashFunction());
						MusicMetadata metadata = toMusicMetadata(jsonObject, this.analysingFile.getAbsolutePath(),hashFuture);
						this.eventThrower.newMusicFile(metadata);
					}
				}
			} catch (InterruptedException | IOException e) {
				log.log(Level.WARNING, this.analysingFile.getAbsolutePath() + " not readable");
			} catch (ExecutionException e) {
				log.log(Level.WARNING, this.analysingFile.getAbsolutePath() + " not readable");
				e.printStackTrace();
			}
		}
	}
		private class BufferedHashFunction implements Callable<byte[]> {
			private int buffersize = 1024;
			
			public BufferedHashFunction( int buffersize ) {
				if ( buffersize > 0 ) {
					this.buffersize = buffersize;
				}
			}
			
			public BufferedHashFunction () {}
			
			@Override
			public byte[] call() throws Exception {
				InputStream stream = Files.newInputStream(analysingFile.toPath(), StandardOpenOption.READ);
				MessageDigest hash = MessageDigest.getInstance("SHA-512");
				byte[] buffer = new byte [buffersize];
				for ( int bytes = stream.read(buffer) ; stream.read(buffer) > 0 ; bytes = stream.read(buffer) ) {
					hash.update(buffer, 0, bytes);
				}
				return hash.digest();
			}
		}
}
