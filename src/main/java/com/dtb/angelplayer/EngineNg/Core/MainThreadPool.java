package com.dtb.angelplayer.EngineNg.Core;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by beij on 02.01.16.
 */
public class MainThreadPool {
    static protected ExecutorService threadPool = Executors.newFixedThreadPool(8);

    public static ExecutorService getThreadPool() {
        return threadPool;
    }
}
